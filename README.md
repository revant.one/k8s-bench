## Bench Kubernetes API

Install site and ingress through HTTP request through event broadcasted.

### Publish Event CreatedNewSiteJobEvent

```
127.0.0.1:6379> PUBLISH CreatedNewSiteJobEvent '{"siteName": "subdomain.domain.com"}'
```

### Subscribe Event

Payload is a JSON like `{ "siteName": "tenant-1.domain.com" }`

```
127.0.0.1:6379> SUBSCRIBE NewSiteCreatedEvent
```

