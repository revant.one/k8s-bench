#!/bin/bash

function checkEnv() {
  if [[ -z "$NODE_ENV" ]]; then
    echo "NODE_ENV is not set"
    exit 1
  fi
  if [[ -z "$ERPNEXT_STABLE_VERSION" ]]; then
    echo "ERPNEXT_STABLE_VERSION is not set"
    exit 1
  fi
  if [[ -z "$ERPNEXT_NEXT_VERSION" ]]; then
    echo "ERPNEXT_NEXT_VERSION is not set"
    exit 1
  fi
  if [[ -z "$ERPNEXT_STABLE_HELM_RELEASE" ]]; then
    echo "ERPNEXT_STABLE_HELM_RELEASE is not set"
    exit 1
  fi
  if [[ -z "$ERPNEXT_NEXT_HELM_RELEASE" ]]; then
    echo "ERPNEXT_NEXT_HELM_RELEASE is not set"
    exit 1
  fi
  if [[ -z "$KUBE_CONFIG" ]]; then
    echo "KUBE_CONFIG is not set"
    exit 1
  fi
  if [[ -z "$ERPNEXT_NAMESPACE" ]]; then
    echo "ERPNEXT_NAMESPACE is not set"
    exit 1
  fi
  if [[ -z "$REDIS_HOST" ]]; then
    echo "REDIS_HOST is not set"
    exit 1
  fi
  if [[ -z "$REDIS_PROTO" ]]; then
    export REDIS_PROTO=redis
  fi
  if [[ -z "$REDIS_PORT" ]]; then
    export REDIS_PORT=6379
  fi
}

function checkConnection() {
  # Wait for services
  echo "Connect Redis . . ."
  timeout 10 bash -c 'until printf "" 2>>/dev/null >>/dev/tcp/$0/$1; do sleep 1; done' $DB_HOST 27017
}

function configureServer() {
  if [ ! -f .env ]; then
    envsubst '${NODE_ENV}
      ${ERPNEXT_STABLE_VERSION}
      ${ERPNEXT_NEXT_VERSION}
      ${ERPNEXT_STABLE_HELM_RELEASE}
      ${ERPNEXT_NEXT_HELM_RELEASE}
      ${ERPNEXT_NAMESPACE}
      ${REDIS_PROTO}
      ${REDIS_HOST}
      ${REDIS_PORT}' \
      < docker/env.tmpl > .env
  fi
}

export -f configureServer

if [ "$1" = 'start' ]; then
  # Validate if DB_HOST is set.
  checkEnv
  # Validate DB Connection
  checkConnection
  # Configure server
  su craft -c "bash -c configureServer"
  su craft -c "node dist/src/main.js"
fi

exec runuser -u craft "$@"
