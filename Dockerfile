FROM node:latest
# Copy and build app
COPY . /home/craft/k8s-bench
WORKDIR /home/craft/
RUN cd k8s-bench \
    && npm install \
    && npm run build \
    && rm -fr node_modules \
    && npm install --only=production

FROM node:slim
# Install dependencies
RUN apt-get update && apt-get install -y gettext-base && apt-get clean -y && rm -rf /var/lib/apt/lists/*

# Setup docker-entrypoint
COPY docker/docker-entrypoint.sh usr/local/bin/docker-entrypoint.sh

# Add non root user and set home directory
RUN useradd -ms /bin/bash craft
WORKDIR /home/craft/k8s-bench
COPY --from=0 /home/craft/k8s-bench .
RUN chown -R craft:craft /home/craft

# Expose port
EXPOSE 7000

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["start"]
