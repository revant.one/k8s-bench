import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { setupRedis } from './redis-server';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  setupRedis(app);
}
bootstrap();
