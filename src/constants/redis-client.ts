import {
  ClientsProviderAsyncOptions,
  RedisOptions,
  Transport,
} from '@nestjs/microservices';
import { ConfigService } from '@nestjs/config';
import {
  REDIS_HOST,
  REDIS_PROTO,
  REDIS_PORT,
} from '../constants/config-options';
import { RETRY_ATTEMPTS, RETRY_DELAY } from '../redis-server';
export const BROADCAST_EVENT = 'BROADCAST_EVENT';

export const redisClient: ClientsProviderAsyncOptions = {
  useFactory: (config: ConfigService): RedisOptions => {
    const url = `${config.get<string>(REDIS_PROTO)}://${config.get<string>(
      REDIS_HOST,
    )}:${config.get<string>(REDIS_PORT)}`;
    return {
      transport: Transport.REDIS,
      options: {
        url,
        retryAttempts: RETRY_ATTEMPTS,
        retryDelay: RETRY_DELAY,
      },
    };
  },
  name: BROADCAST_EVENT,
  inject: [ConfigService],
};
