import { ConfigModuleOptions } from '@nestjs/config/dist/interfaces';
import * as Joi from '@hapi/joi';

export const REDIS_PROTO = 'REDIS_PROTO';
export const REDIS_HOST = 'REDIS_HOST';
export const REDIS_PORT = 'REDIS_PORT';
export const ERPNEXT_NAMESPACE = 'ERPNEXT_NAMESPACE';
export const ERPNEXT_STABLE_VERSION = 'ERPNEXT_STABLE_VERSION';
export const ERPNEXT_STABLE_HELM_RELEASE = 'ERPNEXT_STABLE_HELM_RELEASE';
export const ERPNEXT_NEXT_VERSION = 'ERPNEXT_NEXT_VERSION';
export const ERPNEXT_NEXT_HELM_RELEASE = 'ERPNEXT_NEXT_HELM_RELEASE';

export const configOptions: ConfigModuleOptions = {
  isGlobal: true,
  validationSchema: Joi.object({
    NODE_ENV: Joi.string()
      .valid('development', 'production', 'test', 'staging')
      .default('development'),
    KUBECONFIG: Joi.string().required(),
    [REDIS_PROTO]: Joi.string().default('redis'),
    [REDIS_HOST]: Joi.string().default('redis'),
    [REDIS_PORT]: Joi.string().default('6379'),
    [ERPNEXT_NAMESPACE]: Joi.string().default('erpnext'),
    [ERPNEXT_STABLE_VERSION]: Joi.string().required(),
    [ERPNEXT_STABLE_HELM_RELEASE]: Joi.string().required(),
    [ERPNEXT_NEXT_VERSION]: Joi.string().optional(),
    [ERPNEXT_NEXT_HELM_RELEASE]: Joi.string().optional(),
  }),
};
