import { Module } from '@nestjs/common';
import { ClientsModule } from '@nestjs/microservices';
import { CqrsModule } from '@nestjs/cqrs';

import { KubectlProvider } from './kube-client.provider';
import { IngressConfigProvider } from './ingress-config.provider';
import { CreateSiteAggregateService } from './aggregates/create-site-aggregate/create-site-aggregate.service';
import { KubeController } from './controllers/kube/kube.controller';
import { redisClient } from '../constants/redis-client';
import { KubeEventHandlers } from './events';
import { KubeCommandHandlers } from './commands';

@Module({
  imports: [ClientsModule.registerAsync([redisClient]), CqrsModule],
  controllers: [KubeController],
  providers: [
    ...KubeEventHandlers,
    ...KubeCommandHandlers,
    KubectlProvider,
    IngressConfigProvider,
    CreateSiteAggregateService,
  ],
})
export class KubectlModule {}
