import { Controller, ValidationPipe, UsePipes } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { EventPattern, Payload } from '@nestjs/microservices';
import { CreateNewSiteJobDto } from '../dtos/create-new-site-job.dto';
import { CreatedNewSiteJobEvent } from '../../../constants/app-constants';
import { CreateNewSiteJobCommand } from '../../commands/create-new-site-job/create-new-site-job.command';

@Controller('kube')
export class KubeController {
  constructor(private readonly commandBus: CommandBus) {}

  @EventPattern(CreatedNewSiteJobEvent)
  @UsePipes(ValidationPipe)
  async createNewSiteJobEvent(@Payload() payload: CreateNewSiteJobDto) {
    await this.commandBus.execute(new CreateNewSiteJobCommand(payload));
  }
}
