import { Test, TestingModule } from '@nestjs/testing';
import { CqrsModule } from '@nestjs/cqrs';
import { KubeController } from './kube.controller';

describe('Kube Controller', () => {
  let controller: KubeController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CqrsModule],
      controllers: [KubeController],
    }).compile();

    controller = module.get<KubeController>(KubeController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
