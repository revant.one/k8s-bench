import { IsUrl, IsString, IsOptional, IsBoolean } from 'class-validator';

export class CreateNewSiteJobDto {
  @IsUrl()
  siteName: string;

  @IsBoolean()
  @IsOptional()
  stable: boolean;

  @IsOptional()
  @IsString()
  language: string;

  @IsOptional()
  @IsString()
  country: string;

  @IsOptional()
  @IsString()
  timezone: string;

  @IsOptional()
  @IsString()
  currency: string;

  @IsOptional()
  @IsString()
  fullName: string;

  @IsOptional()
  @IsString()
  email: string;

  @IsOptional()
  @IsString()
  password: string;

  @IsOptional()
  @IsString()
  domains: string;

  @IsOptional()
  @IsString()
  companyName: string;

  @IsOptional()
  @IsString()
  companyAbbr: string;

  @IsOptional()
  @IsString()
  companyTagline: string;

  @IsOptional()
  @IsString()
  bankAccount: string;

  @IsOptional()
  @IsString()
  chartOfAccounts: string;

  @IsOptional()
  @IsString()
  fyStartDate: string;

  @IsOptional()
  @IsString()
  fyEndDate: string;
}
