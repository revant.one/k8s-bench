import { Provider } from '@nestjs/common';
import * as k8s from '@kubernetes/client-node';

export const KUBE_CONFIG = 'KUBE_CONFIG';
export const KubectlProvider: Provider = {
  provide: KUBE_CONFIG,
  useFactory: async () => {
    const kubeConfig = new k8s.KubeConfig();
    kubeConfig.loadFromDefault();
    return kubeConfig;
  },
};
