import { V1Job } from '@kubernetes/client-node';
import {
  ERPNEXT_WORKER_IMAGE,
  ERPNEXT,
  SITES_DIR,
  WIZARD_SCRIPT,
} from '../../constants/app-constants';
import { CreateNewSiteJobDto } from '../controllers/dtos/create-new-site-job.dto';

export const CREATE_NEW_SITE = 'create-new-site';
export const RUN_WIZARD = 'run-wizard';

export const generateSiteJobJsonTemplate = (
  wizard: CreateNewSiteJobDto,
  namespace: string,
  version: string,
  pvc: string,
): V1Job => {
  return {
    metadata: {
      name: `${CREATE_NEW_SITE}-${wizard.siteName}`,
      namespace,
    },
    spec: {
      backoffLimit: 1,
      template: {
        spec: {
          securityContext: {
            supplementalGroups: [1000],
          },
          initContainers: [
            {
              name: CREATE_NEW_SITE,
              image: `${ERPNEXT_WORKER_IMAGE}:${version}`,
              args: ['new'],
              imagePullPolicy: 'IfNotPresent',
              volumeMounts: [
                {
                  name: SITES_DIR,
                  mountPath: '/home/frappe/frappe-bench/sites',
                },
              ],
              env: [
                { name: 'SITE_NAME', value: wizard.siteName },
                { name: 'DB_ROOT_USER', value: 'root' },
                {
                  name: 'MYSQL_ROOT_PASSWORD',
                  valueFrom: {
                    secretKeyRef: {
                      key: 'password',
                      name: 'mariadb-root-password',
                    },
                  },
                },
                {
                  name: 'ADMIN_PASSWORD',
                  valueFrom: {
                    secretKeyRef: {
                      key: 'password',
                      name: `${wizard.siteName}-admin-password`,
                    },
                  },
                },
                { name: 'INSTALL_APP', value: ERPNEXT.toLowerCase() },
              ],
            },
          ],
          containers: [
            {
              name: RUN_WIZARD,
              image: `${ERPNEXT_WORKER_IMAGE}:${version}`,
              command: ['/home/frappe/frappe-bench/env/bin/python'],
              args: ['/opt/frappe/wizard.py'],
              imagePullPolicy: 'IfNotPresent',
              volumeMounts: [
                {
                  name: SITES_DIR,
                  mountPath: '/home/frappe/frappe-bench/sites',
                },
                {
                  name: WIZARD_SCRIPT,
                  mountPath: '/opt/frappe',
                },
              ],
              env: [
                { name: 'LANGUAGE', value: wizard.language },
                { name: 'COUNTRY', value: wizard.country },
                { name: 'TIMEZONE', value: wizard.timezone },
                { name: 'CURRENCY', value: wizard.currency },
                { name: 'FULL_NAME', value: wizard.fullName },
                { name: 'EMAIL', value: wizard.email },
                { name: 'PASSWORD', value: wizard.password },
                { name: 'DOMAINS', value: wizard.domains },
                { name: 'COMPANY_NAME', value: wizard.companyName },
                { name: 'COMPANY_ABBR', value: wizard.companyAbbr },
                { name: 'COMPANY_TAGLINE', value: wizard.companyTagline },
                { name: 'BANK_ACCOUNT', value: wizard.bankAccount },
                { name: 'CHART_OF_ACCOUNTS', value: wizard.chartOfAccounts },
                { name: 'FY_START_DATE', value: wizard.fyStartDate },
                { name: 'FY_END_DATE', value: wizard.fyEndDate },
              ],
            },
          ],
          restartPolicy: 'Never',
          volumes: [
            {
              name: SITES_DIR,
              persistentVolumeClaim: {
                claimName: pvc,
                readOnly: false,
              },
            },
            {
              name: WIZARD_SCRIPT,
              configMap: {
                name: WIZARD_SCRIPT,
              },
            },
          ],
        },
      },
    },
  };
};
