import { NetworkingV1beta1Ingress } from '@kubernetes/client-node';
import { IngressConfig } from '../ingress-config.provider';

export const generateIngressTemplate = (
  siteName: string,
  serviceName: string,
  ingressConfig: IngressConfig,
): NetworkingV1beta1Ingress => {
  return {
    metadata: {
      name: `${siteName}`,
      labels: ingressConfig.labels,
      annotations: ingressConfig.annotations,
    },
    spec: {
      rules: [
        {
          host: siteName,
          http: {
            paths: [
              {
                backend: {
                  serviceName,
                  servicePort: 80 as any,
                },
                path: '/',
              },
            ],
          },
        },
      ],
      tls: [
        {
          hosts: [siteName],
          secretName: `${siteName}-tls`,
        },
      ],
    },
  };
};
