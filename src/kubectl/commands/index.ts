import { CreateNewSiteJobHandler } from './create-new-site-job/create-new-site-job.handler';

export const KubeCommandHandlers = [CreateNewSiteJobHandler];
