import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { CreateNewSiteJobCommand } from './create-new-site-job.command';
import { CreateSiteAggregateService } from '../../aggregates/create-site-aggregate/create-site-aggregate.service';

@CommandHandler(CreateNewSiteJobCommand)
export class CreateNewSiteJobHandler
  implements ICommandHandler<CreateNewSiteJobCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: CreateSiteAggregateService,
  ) {}

  async execute(command: CreateNewSiteJobCommand) {
    const { payload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.createSiteJob(payload).toPromise();
    aggregate.commit();
  }
}
