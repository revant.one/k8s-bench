import { NewSiteCreatedHandler } from './new-site-created/new-site-created.handler';

export const KubeEventHandlers = [NewSiteCreatedHandler];
