import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Inject } from '@nestjs/common';
import { ClientRedis } from '@nestjs/microservices';
import { NewSiteCreatedEvent } from './new-site-created.event';
import { BROADCAST_EVENT } from '../../../constants/redis-client';

@EventsHandler(NewSiteCreatedEvent)
export class NewSiteCreatedHandler implements IEventHandler {
  constructor(
    @Inject(BROADCAST_EVENT) private readonly broadcast: ClientRedis,
  ) {}
  handle(event: NewSiteCreatedEvent) {
    const { siteName } = event;
    this.broadcast.emit(event.constructor.name, event);
    return siteName;
  }
}
