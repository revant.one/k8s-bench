import { IEvent } from '@nestjs/cqrs';

export class NewSiteCreatedEvent implements IEvent {
  constructor(public readonly siteName: string) {}
}
