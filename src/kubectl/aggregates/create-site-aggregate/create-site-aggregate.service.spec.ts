import { Test, TestingModule } from '@nestjs/testing';
import { KUBE_CONFIG } from '../../kube-client.provider';
import { CreateSiteAggregateService } from './create-site-aggregate.service';
import { INGRESS_CONFIG } from '../../ingress-config.provider';
import { ConfigService } from '@nestjs/config';

describe('CreateSiteAggregateService', () => {
  let service: CreateSiteAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CreateSiteAggregateService,
        { provide: INGRESS_CONFIG, useValue: {} },
        { provide: KUBE_CONFIG, useValue: {} },
        { provide: ConfigService, useValue: {} },
      ],
    }).compile();

    service = module.get<CreateSiteAggregateService>(
      CreateSiteAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
