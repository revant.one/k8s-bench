import { Injectable, Inject } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AggregateRoot } from '@nestjs/cqrs';
import { randomBytes } from 'crypto';
import { from, throwError } from 'rxjs';
import { switchMap, delay, map } from 'rxjs/operators';
import { V1Job } from '@kubernetes/client-node';
import * as k8s from '@kubernetes/client-node';

import { KUBE_CONFIG } from '../../kube-client.provider';
import {
  generateSiteJobJsonTemplate,
  CREATE_NEW_SITE,
} from '../../resources/create-site.k8s-job';
import {
  ERPNEXT_NAMESPACE,
  ERPNEXT_STABLE_HELM_RELEASE,
  ERPNEXT_NEXT_HELM_RELEASE,
  ERPNEXT_STABLE_VERSION,
  ERPNEXT_NEXT_VERSION,
} from '../../../constants/config-options';
import { CreateSiteJobFailedError } from '../../../constants/exceptions';
import {
  ERPNEXT,
  JOB_CREATION_FAILED,
  TWENTY_MINUTES_NUMBER,
  JOB_CREATION_TIMEOUT,
  TWO_MINUTES_IN_MILLISECONDS_NUMBER,
  JOB_NOT_FOUND,
} from '../../../constants/app-constants';
import { generateSecretTemplate } from '../../resources/create-secret.k8s-secret';
import { CreateNewSiteJobDto } from '../../controllers/dtos/create-new-site-job.dto';
import { generateIngressTemplate } from '../../resources/create-ingress.k8s-ingress';
import { INGRESS_CONFIG, IngressConfig } from '../../ingress-config.provider';
import { NewSiteCreatedEvent } from '../../events/new-site-created/new-site-created.event';

@Injectable()
export class CreateSiteAggregateService extends AggregateRoot {
  constructor(
    @Inject(KUBE_CONFIG)
    private readonly kubeConfig: k8s.KubeConfig,
    @Inject(INGRESS_CONFIG)
    private readonly ingressConfig: IngressConfig,
    private readonly config: ConfigService,
  ) {
    super();
  }

  createSiteJob(payload: CreateNewSiteJobDto) {
    const batchV1Api = this.kubeConfig.makeApiClient(k8s.BatchV1Api);
    const watchApi = new k8s.Watch(this.kubeConfig);
    const coreV1Api = this.kubeConfig.makeApiClient(k8s.CoreV1Api);

    const namespace = this.config.get<string>(ERPNEXT_NAMESPACE);
    const version = payload.stable
      ? this.config.get<string>(ERPNEXT_STABLE_VERSION)
      : this.config.get<string>(ERPNEXT_NEXT_VERSION);
    const pvcAndSvcName = payload.stable
      ? `${this.config.get<string>(
          ERPNEXT_STABLE_HELM_RELEASE,
        )}-${ERPNEXT.toLowerCase()}`
      : `${this.config.get<string>(
          ERPNEXT_NEXT_HELM_RELEASE,
        )}-${ERPNEXT.toLowerCase()}`;

    const adminPasswordBase64 = Buffer.from(
      randomBytes(32).toString('hex'),
    ).toString('base64');

    const secretBody = generateSecretTemplate(
      adminPasswordBase64,
      payload.siteName,
    );

    const jobBody = generateSiteJobJsonTemplate(
      payload,
      namespace,
      version,
      pvcAndSvcName,
    );

    return from(coreV1Api.createNamespacedSecret(namespace, secretBody)).pipe(
      switchMap(() => {
        return from(batchV1Api.createNamespacedJob(namespace, jobBody));
      }),
      switchMap(() =>
        this.watchJobCompletion(
          jobBody.metadata.name,
          pvcAndSvcName,
          namespace,
          watchApi,
          batchV1Api,
        ),
      ),
    );
  }

  watchJobCompletion(
    jobName: string,
    pvcAndSvcName: string,
    namespace: string,
    watchApi: k8s.Watch,
    batchV1Api: k8s.BatchV1Api,
  ) {
    return from(
      watchApi.watch(
        `/apis/batch/v1/namespaces/${namespace}/jobs`,
        { allowWatchBookmarks: true },
        () => {},
        () => {},
      ),
    ).pipe(
      delay(TWO_MINUTES_IN_MILLISECONDS_NUMBER),
      switchMap(req => {
        return from(
          batchV1Api.listNamespacedJob(
            namespace,
            undefined,
            undefined,
            undefined,
            `metadata.name=${jobName}`,
          ),
        ).pipe(
          switchMap(({ body }) => {
            if (body.items.length === 0) {
              req.abort();
              return throwError(new CreateSiteJobFailedError(JOB_NOT_FOUND));
            }

            if (body.items.length === 1) {
              const job = body.items[0];
              const expiry = new Date(job.status.startTime);
              expiry.setMinutes(expiry.getMinutes() + TWENTY_MINUTES_NUMBER);

              if (job.status.succeeded) {
                req.abort();
                return this.createIngress(job, pvcAndSvcName);
              } else if (job.status.failed) {
                req.abort();
                return throwError(
                  new CreateSiteJobFailedError(JOB_CREATION_FAILED),
                );
              } else if (new Date() > expiry) {
                req.abort();
                return throwError(
                  new CreateSiteJobFailedError(JOB_CREATION_TIMEOUT),
                );
              } else {
                return this.watchJobCompletion(
                  jobName,
                  pvcAndSvcName,
                  namespace,
                  watchApi,
                  batchV1Api,
                );
              }
            }
          }),
        );
      }),
    );
  }

  createIngress(job: V1Job, serviceName: string) {
    const siteName = job.metadata.name.replace(`${CREATE_NEW_SITE}-`, '');
    const networkingV1Beta1Api = this.kubeConfig.makeApiClient(
      k8s.NetworkingV1beta1Api,
    );
    const ingressBody = generateIngressTemplate(
      siteName,
      serviceName,
      this.ingressConfig,
    );
    return from(
      networkingV1Beta1Api.createNamespacedIngress(
        job.metadata.namespace,
        ingressBody,
      ),
    ).pipe(
      map(data => {
        this.apply(new NewSiteCreatedEvent(siteName));
        return data;
      }),
    );
  }
}
