import { Provider } from '@nestjs/common';
import { promises } from 'fs';
import { ConfigService } from '@nestjs/config';
import { ERPNEXT_STABLE_HELM_RELEASE } from '../constants/config-options';

export const INGRESS_CONFIG = 'INGRESS_CONFIG';
export const INGRESS_CONFIG_JSON = 'ingress-config.json';
export interface IngressConfig {
  labels?: { [key: string]: string };
  annotations?: { [key: string]: string };
}

export const IngressConfigProvider: Provider = {
  provide: INGRESS_CONFIG,
  useFactory: async (configService: ConfigService): Promise<IngressConfig> => {
    const clusterIssuer = 'letsencrypt-prod';
    const helmRelease = configService.get(ERPNEXT_STABLE_HELM_RELEASE);
    const config: IngressConfig = {
      labels: {
        'app.kubernetes.io/instance': `${helmRelease}-erpnext`,
      },
      annotations: {
        'cert-manager.io/cluster-issuer': clusterIssuer,
      },
    };

    try {
      const jsonFile = await promises.readFile(INGRESS_CONFIG_JSON);
      return JSON.parse(jsonFile.toString()) as IngressConfig;
    } catch (error) {}

    return config;
  },
  inject: [ConfigService],
};
