import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { KubectlModule } from './kubectl/kubectl.module';
import { configOptions } from './constants/config-options';

@Module({
  imports: [ConfigModule.forRoot(configOptions), KubectlModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
