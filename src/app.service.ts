import { Injectable } from '@nestjs/common';
import { SERVICE_NAME } from './constants/app-constants';

@Injectable()
export class AppService {
  getServiceName(): { service: string } {
    return { service: SERVICE_NAME };
  }
}
