import { INestApplication, Logger } from '@nestjs/common';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { ConfigService } from '@nestjs/config';
import {
  REDIS_HOST,
  REDIS_PROTO,
  REDIS_PORT,
} from './constants/config-options';

const LISTENING_TO_EVENTS = 'Listening to events using Redis';

export const RETRY_ATTEMPTS = 3;
export const RETRY_DELAY = 10;

export function setupRedis(app: INestApplication) {
  const config = app.get<ConfigService>(ConfigService);
  const url = `${config.get<string>(REDIS_PROTO)}://${config.get<string>(
    REDIS_HOST,
  )}:${config.get<string>(REDIS_PORT)}`;
  const redis = app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.REDIS,
    options: {
      url,
      retryAttempts: RETRY_ATTEMPTS,
      retryDelay: RETRY_DELAY,
    },
  });
  redis.listen(() => Logger.log(LISTENING_TO_EVENTS, redis.constructor.name));
}
